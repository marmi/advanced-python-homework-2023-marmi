# advanced-python-homework-2023

# Окружение

Чтобы развернуть окружение для разработки с помощью conda:

    conda env create -f environment.yml

    conda activate scada-marmi

# Sphinx

    sphinx-quickstart docs

    sphinx-build -M html docs/source/ docs/build/

    cd docs

    ./make html
